package com.jjara.utils;

/**
 *
 * @Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 */
public class StringUtils {

    public static boolean isNumber(Object val) {
        boolean isNumber;
        try {
            Integer.parseInt(val.toString());
            isNumber = true;
        } catch (NumberFormatException e) {
            isNumber = false;
        }
        return isNumber;
    }
}
