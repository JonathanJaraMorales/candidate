package com.jjara.enums;

/**
 *
 * @author Jonathan Jar Morales <jonathan.jara.morales@gmail.com>
 */
public enum RomanEnum {

    ONES(0),
    TENS(1),
    HUNDREDS(2),
    THOUSANDS(3);

    private final int number;

    private RomanEnum(int number) {
        this.number = number;
    }

    public int getType() {
        return number;
    }
}
