package com.jjara.controller;

import com.jjara.enums.RomanEnum;
import com.jjara.interfaces.RomanNumeralConverter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 */
public class RomanController implements Serializable, RomanNumeralConverter {

    private final Integer LIMIT = 4000;
    private HashMap<Character, Integer> numToInt;

    public String toRomanNumeral(int number) {
        StringBuilder result = new StringBuilder();
        ArrayList<Integer> array = fillArray(number);
        int size = array.size();
        for (int i = 0; i < size; i++) {
            if (i == RomanEnum.ONES.getType()) {
                result.insert(0, getOnes(array));
            } else if (i == RomanEnum.TENS.getType()) {
                result.insert(0, getTens(array));
            } else if (i == RomanEnum.HUNDREDS.getType()) {
                result.insert(0, getHundreds(array));
            } else if (i == RomanEnum.THOUSANDS.getType()) {
                result.insert(0, getThousands(array));
            }
        }
        return result.toString();
    }
    
      public int fromRomanNumeral(String romanNumeral) {
        int decimal = 0;
        int lastNumber = 0;
        String result = romanNumeral.toUpperCase();
        for (int x = result.length() - 1; x >= 0; x--) {
            char convertToDecimal = result.charAt(x);
 
            switch (convertToDecimal) {
                case 'M':
                    decimal = CheckRoman(1000, lastNumber, decimal);
                    lastNumber = 1000;
                    break;
 
                case 'D':
                    decimal = CheckRoman(500, lastNumber, decimal);
                    lastNumber = 500;
                    break;
 
                case 'C':
                    decimal = CheckRoman(100, lastNumber, decimal);
                    lastNumber = 100;
                    break;
 
                case 'L':
                    decimal = CheckRoman(50, lastNumber, decimal);
                    lastNumber = 50;
                    break;
 
                case 'X':
                    decimal = CheckRoman(10, lastNumber, decimal);
                    lastNumber = 10;
                    break;
 
                case 'V':
                    decimal = CheckRoman(5, lastNumber, decimal);
                    lastNumber = 5;
                    break;
 
                case 'I':
                    decimal = CheckRoman(1, lastNumber, decimal);
                    lastNumber = 1;
                    break;
            }
        }
        return decimal;
    }
      
      public int CheckRoman(int TotalDecimal, int LastRomanLetter, int LastDecimal) {
        if (LastRomanLetter > TotalDecimal) {
            return LastDecimal - TotalDecimal;
        } else {
            return LastDecimal + TotalDecimal;
        }
    }
        
    private String getThousands(ArrayList<Integer> array){
        String result = "";
        switch (array.get(3).toString()){           
            case "0" : result = ""; break;
            case "1" : result = "M"; break;
            case "2" : result = "MM"; break;
            case "3" : result = "MMM"; break;
        }
        return result; 
    }
    
    private String getHundreds(ArrayList<Integer> array){
        String result = "";
        switch (array.get(2).toString()){
           case "0" : result = ""; break;
           case "1" : result = "C"; break;
           case "2" : result = "CC"; break;
           case "3" : result = "CCC"; break;
           case "4" : result = "CD"; break;
           case "5" : result = "D"; break;
           case "6" : result = "DC"; break;
           case "7" : result = "DCC"; break;
           case "8" : result = "DCCC"; break;
           case "9" : result = "CM"; break;
       }
       return result;
    }

    private String getTens(ArrayList<Integer> array) {
        String result = "";
        switch (array.get(1).toString()) {
            case "0": result = ""; break;
            case "1": result = "X"; break;
            case "2": result = "XX"; break;
            case "3": result = "XXX"; break;
            case "4": result = "XL"; break;
            case "5": result = "L"; break;
            case "6": result = "LX"; break;
            case "7": result = "LXX"; break;
            case "8": result = "LXXX"; break;
            case "9": result = "XC"; break;
        }
        return result;
    }

    private String getOnes(ArrayList<Integer> array) {
        String result = "";
        switch (array.get(0).toString()) {
            case "0": result = ""; break;
            case "1": result = "I"; break;
            case "2": result = "II"; break;
            case "3": result = "III"; break;
            case "4": result = "IV"; break;
            case "5": result = "V"; break;
            case "6": result = "VI"; break;
            case "7": result = "VII"; break;
            case "8": result = "VIII"; break;
            case "9": result = "IX"; break;
        }        
        return result;
    }

    private ArrayList<Integer> fillArray(int number) {
        ArrayList<Integer> array = new ArrayList<>();
        boolean result = true;
        while (number / 10 != 10 && result) {
            if (number / 10 != 0 && number < LIMIT) {
                array.add(number % 10);
                number = number / 10;
            } else {
                result = false;
            }
        }
        array.add(number);
        return array;
    }

    

    public void setup() {
        numToInt = new HashMap<>();
        String numerals = "MDCLXVI";
        int number = 1000;
        int factor = 2;
        for (char numeral : numerals.toCharArray()) {
            numToInt.put(numeral, number);
            number /= factor;
            factor = (factor == 2 ? 5 : 2);
        }
    }
}
