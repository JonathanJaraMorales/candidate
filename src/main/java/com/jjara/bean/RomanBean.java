/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjara.bean;

import com.jjara.controller.RomanController;
import com.jjara.utils.StringUtils;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import static javax.faces.context.FacesContext.getCurrentInstance;

/**
 *
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 */
@ManagedBean(name = "RomanBean")
@ViewScoped
public class RomanBean implements Serializable {

    private int minimum = 0;
    private int maximum = 3999;

    private String romanNumber;
    private int decimalNumber;

    private String romanResult = null;
    private String decimalResult;

    public void fromRomanNumeral() {
        if (!StringUtils.isNumber(romanNumber)) {
            setDecimalNumber(new RomanController().fromRomanNumeral(romanNumber));
        } else {
            getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Only letters are allowed", ""));
        }
    }

    public void toRomanNumeral() {
        if (StringUtils.isNumber(decimalNumber)) {
            if (decimalNumber == 0) {
                getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Zero is not allowed", ""));
            } else {
                setRomanResult(new RomanController().toRomanNumeral(decimalNumber));
            }
        } else {
            getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Only numbers are allowed", ""));
        }

    }

    public int getDecimalNumber() {
        return decimalNumber;
    }

    public void setDecimalNumber(int roman) {
        this.decimalNumber = roman;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    public int getMaximum() {
        return maximum;
    }

    public void setMaximum(int maximum) {
        this.maximum = maximum;
    }

    public String getRomanResult() {
        return romanResult;
    }

    public void setRomanResult(String romanResult) {
        this.romanResult = romanResult;
    }

    public String getRomanNumber() {
        return romanNumber;
    }

    public void setRomanNumber(String romanNumber) {
        this.romanNumber = romanNumber;
    }

    /**
     * @return the decimalResult
     */
    public String getDecimalResult() {
        return decimalResult;
    }

    /**
     * @param decimalResult the decimalResult to set
     */
    public void setDecimalResult(String decimalResult) {
        this.decimalResult = decimalResult;
    }

}
