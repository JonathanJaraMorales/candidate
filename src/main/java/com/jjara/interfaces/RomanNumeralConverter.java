package com.jjara.interfaces;

/**
 *
 * @author Jonathan Jara Morales <jonathan.jara.morales@gmail.com>
 */
public interface RomanNumeralConverter {

    public int fromRomanNumeral(String romanNumeral);

    public String toRomanNumeral(int number);
}
