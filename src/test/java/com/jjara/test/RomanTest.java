/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jjara.test;

import com.jjara.bean.RomanBean;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jonathan
 */
public class RomanTest {

    public RomanTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void fromRomanNumeral() {
        RomanBean bean = new RomanBean();
        bean.setRomanNumber("XX");
        bean.fromRomanNumeral();
        int result = bean.getDecimalNumber();
        assertEquals(result, 20);
    }

    @Test
    public void toRomanNumeral() {
        RomanBean bean = new RomanBean();
        bean.setDecimalNumber(20);
        bean.toRomanNumeral();
        String result = bean.getRomanResult();
        assertEquals(result, "XX");
    }
}
